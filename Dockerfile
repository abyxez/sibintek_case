FROM python:latest

RUN apt-get update &&\
    apt-get upgrade -y &&\
    apt-get install -y libpq-dev gcc netcat-traditional

WORKDIR /app

RUN pip install -U pip

COPY . .

CMD [ "python3", "./main.py" ]